
const wording = ["Free Shipping Over £100", "Pay in 3 With PayPal", "Trustpilot - 3.6/5", "30 Day Hassle Free Returns"]
/* const createArrayFromDivElements = Array.from(document.querySelectorAll('.propositionBarDesktop > div > h3'))

const wording = createArrayFromDivElements.map(word => {
    return word.innerText;
}) */

const change = document.querySelector('.change');
const desktop = document.querySelector('.propositionBarDesktop');

let speed = 3000;
let count = 0;
let index = 1;

let regExFraction = /\d{0,2}.\d\/\d/ig
changeFun();
setInterval(() => changeFun(), speed);

function changeFun() {
    
    change.classList.add('fade');

    setTimeout(function(){

    if(wording[index].toLocaleLowerCase().includes('trustpilot')){

        const fraction = findFranction(wording[index])
        const stars = createStars(+fraction[0])

        change.innerHTML = `<h3>${wording[index]} ${stars.join("")}</h3>`;

    } else{
        change.innerHTML = `<h3>${wording[index]}</h3>`;
    }
   
    change.classList.remove('fade');
    index++;

        if(index > wording.length - 1){
            index = 0;
        }

    }, 500);
}

const displayDesktop = () => {
    let html = wording.map((word, index) => {
       
        if(wording[index].toLocaleLowerCase().includes('trustpilot')){
            const fraction = findFranction(word);
            const stars = createStars(+fraction[0])
            word = `${word} ${stars.join("")}`;
        }

        if(index > wording.length - 2){
            return `<div><h3>${word}</h3></div>`;
        } else {
            return `<div><h3>${word}</h3></div><div class="verticalLine"></div>`;
        }       
    })
    desktop.innerHTML = html.join("");
}

const createStars = (numToMake) => {
    let array = new Array(Math.floor(numToMake)).fill(`<i class="fa fa-star" aria-hidden="true"></i>`);
    if (numToMake > Math.floor(numToMake) + 0.4) {
        array.push(`<i class="fa fa-star-half" aria-hidden="true"></i>`)
    }
    return array;
}

const findFranction = (word) => {
    let matched = word.match(regExFraction);
    return matched[0].split("/");
}


displayDesktop();